package com.example.bankseckill;

import com.example.bankseckill.mapper.OrderMapper;
import com.example.bankseckill.pojo.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.Date;

@SpringBootTest
class BankSeckillApplicationTests {

    @Autowired
    OrderMapper orderMapper;

    @Test
    void contextLoads() {
    }

    @Test
    public void  test(){
        Order order = new Order();
        long start = System.currentTimeMillis();
        for (int i=0;i<1000;i++){
            order.setUserId(12205L);
            order.setStatus(0);
            order.setProductPrice(BigDecimal.valueOf(1000));
            order.setProductName("123");
            order.setProductId(1L);
            order.setPayDate(new Date());
            order.setProductCount(1);
            orderMapper.insert(order);
        }
        long end = System.currentTimeMillis();
        System.out.println(end-start);
    }

}
