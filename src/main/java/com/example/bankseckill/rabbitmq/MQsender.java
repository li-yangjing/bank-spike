package com.example.bankseckill.rabbitmq;

import com.example.bankseckill.pojo.SecKillMessageConstants;
import com.example.bankseckill.pojo.SeckillOrder;
import com.example.bankseckill.pojo.SeckillmessageLog;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Date;

/**
 * @Author: liyangjing
 * @Date: 2022/02/25/14:11
 * @Description:
 *
 * mq 发送者
 */

@Service
@Slf4j
public class MQsender {

    @Autowired
    private RabbitTemplate rabbitTemplate;


    public void sendSeckillMessage(SeckillmessageLog seckillmessageLog, String msgId){
        log.info("发送消息"+seckillmessageLog);
        rabbitTemplate.convertAndSend(SecKillMessageConstants.EXCHANGE_NAME,SecKillMessageConstants.ROUTING_KEY_NAME,seckillmessageLog,new CorrelationData(msgId));
    }

    /**
     * 延迟队列 半个小时取消订单
     * @param seckillOrder
     */
    public void OrderDelayQueue(SeckillOrder seckillOrder){
        rabbitTemplate.convertAndSend(SecKillMessageConstants.DELAYED_EXCHANGE_NAME,SecKillMessageConstants.DELAYED_ROUTING_KEY, seckillOrder, correlationData ->{
            correlationData.getMessageProperties().setDelay(60000);
            return correlationData;
        },new CorrelationData("order"));
        log.info(" 当 前 时 间 ： {}, 发送一条延迟 {} 毫秒的信息给队列 delayed.queue:{}", new
                Date(),30000, seckillOrder);
    }
}
