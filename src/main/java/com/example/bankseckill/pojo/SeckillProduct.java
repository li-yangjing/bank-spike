package com.example.bankseckill.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.Future;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author liyangjing
 * @since 2022-01-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_seckill_product")
@ApiModel(value="SeckillProduct对象", description="")
public class SeckillProduct implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "秒杀商品id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "产品id")
    @TableField("product_id")
    @NotNull
    private Long productId;

    @ApiModelProperty(value = "秒杀年利率")
    @TableField("seckill_interest_rate")
    private Float seckillInterestRate;

    @ApiModelProperty(value = "产品库存")
    @TableField("stock_count")
    @NotNull
    private Integer stockCount;

    @ApiModelProperty(value = "开始时间")
    @TableField("start_date")
    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "Asia/Shanghai")
    private Date startDate;

    @ApiModelProperty(value = "结束时间")
    @TableField("end_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "Asia/Shanghai")
    @NotNull
    @Future(message = "-活动结束时间大于现在的时间")
    private Date endDate;

    @ApiModelProperty(value = "活动状态，1启用，0禁用")
    @Max(value = 1,message = "开启状态 1启用，0禁用")
    private Integer enable;

    @ApiModelProperty(value = "配置待查询的逾期年限。eg：3，表示查询近3年用户的逾期情况")
    @TableField("violate_years")
    private Integer violateYears;

    @ApiModelProperty(value = "配置待查询的逾期次数。eg: 2，表示逾期2次及以上就可能被拒绝")
    @TableField("violate_count")
    private Integer violateCount;

    @ApiModelProperty(value = "配置允许的最大逾期金额，eg：1000，表示金额小于等于1000元的可能被接受(需要在violate_days内还完)，大于1000元绝对不能被接受")
    @TableField("violate_money")
    private Integer violateMoney;

    @ApiModelProperty(value = "配置最大允许的逾期时间，eg：3，表示如果3天之内还完逾期金额，那么就能被接受(需要还完violate_money)。如果3天之外的话，那么就被拒绝。")
    @TableField("violate_days")
    private Integer violateDays;

    @ApiModelProperty(value = "配置允许的用户年龄，eg：18，表示只有大于等于18岁的用户才能被接受，否则就拒绝")
    @TableField("min_age")
    private Integer minAge;
    /**
     * 原价商品信息
     */
    @ApiModelProperty(value = "产品")
    @TableField(exist = false)
    private Product product;

}
