package com.example.bankseckill.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author liyangjing
 * @since 2022-01-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_product")
@ApiModel(value="Product对象", description="")
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "产品id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "产品名称")
    @TableField("product_name")
    @NotBlank
    private String productName;

    @ApiModelProperty(value = "产品期限")
    @TableField("product_duration")
    private Integer productDuration;

    @ApiModelProperty(value = "年利化率")
    @TableField("annual_interest_rate")
    private Float annualInterestRate;

    @ApiModelProperty(value = "起存金额")
    @TableField("deposit_amount")
    private BigDecimal depositAmount;

    @ApiModelProperty(value = "递增金额")
    @TableField("increment_amount")
    private BigDecimal incrementAmount;

    @ApiModelProperty(value = "单人限额")
    @TableField("single_person_limit")
    private BigDecimal singlePersonLimit;

    @ApiModelProperty(value = "单日限额")
    @TableField("single_day_limit")
    private BigDecimal singleDayLimit;

    @ApiModelProperty(value = "风险等级，低0，中1，高2")
    @TableField("risk_level")
    private Integer riskLevel;

    @ApiModelProperty(value = "起息日")
    @TableField("value_date")
    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "Asia/Shanghai")
    private Date valueDate;

    @ApiModelProperty(value = "到期日")
    @TableField("expiry_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "Asia/Shanghai")
    private Date expiryDate;

    @ApiModelProperty(value = "图片")
    @TableField("picture")
    private String picture;

}
