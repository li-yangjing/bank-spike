package com.example.bankseckill.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author liyangjing
 * @since 2022-01-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_seckill_order")
@ApiModel(value="SeckillOrder对象", description="")
public class SeckillOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "秒杀订单id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "用户id")
    @TableField("user_id")
    private Long userId;

    @ApiModelProperty(value = "用户姓名")
    @TableField(exist = false)
    private String name;

    @ApiModelProperty(value = "用户电话")
    @TableField(exist = false)
    private String phone;

    @ApiModelProperty(value = "用户邮箱")
    @TableField(exist = false)
    private String email;

    @ApiModelProperty(value = "订单id")
    @TableField("order_id")
    private Long orderId;

    @ApiModelProperty(value = "订单信息")
    @TableField(exist = false)
    private Order order;

    @ApiModelProperty(value = "产品id")
    @TableField("product_id")
    private Long productId;

    /**
     * 秒杀商品信息
     */
    @ApiModelProperty(value = "秒杀产品信息")
    @TableField(exist = false)
    private SeckillProduct seckillProduct;

}
