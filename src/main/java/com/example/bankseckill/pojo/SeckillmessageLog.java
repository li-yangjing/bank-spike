package com.example.bankseckill.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author Liyangjing
 * @since 2022-03-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_seckillmessage_log")
@ApiModel(value="消息日志对象", description="")
public class SeckillmessageLog implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 消息id
     */
    @TableId("msgId")
    @ApiModelProperty(value = "消息id")
    private String msgId;

    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id")
    @TableField("userId")
    private Long userId;

    /**
     *  秒杀产品id
     */
    @ApiModelProperty(value = "秒杀产品id")
    @TableField("secproductId")
    private Long secproductId;

    /**
     * 状态（0：消息投递中 1：投递成功 2：投递失败）
     */
    @ApiModelProperty(value = "消息状态")
    @TableField("status")
    private Integer status;

    /**
     * 路由键
     */
    @ApiModelProperty(value = "路由键")
    @TableField("routeKey")
    private String routeKey;

    /**
     * 交换机
     */
    @ApiModelProperty(value = "交换机")
    @TableField("exchange")
    private String exchange;

    /**
     * 重试次数
     */
    @ApiModelProperty(value = "重试次数")
    @TableField("count")
    private Integer count;

    /**
     * 重试时间
     */
    @ApiModelProperty(value = "重试时间")
    @TableField("tryTime")
    private LocalDateTime tryTime;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField("createTime")
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField("updateTime")
    private LocalDateTime updateTime;

}
