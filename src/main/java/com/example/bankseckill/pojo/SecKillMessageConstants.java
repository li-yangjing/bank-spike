package com.example.bankseckill.pojo;

/**
 * 消息状态
 */
public class SecKillMessageConstants {
    //消息投递中
    public static final Integer DELIVERING = 0;
    //消息投递成功
    public static final Integer SUCCESS = 1;
    //消息投递失败
    public static final Integer FAILURE = 2;
    //最大重试次数
    public static final Integer MAX_TRY_COUNT = 3;
    //消息超时时间
    public static final Integer MSG_TIMEOUT = 1;
    //队列
    public static final String QUEUE_NAME = "seckill.Queue";
    //交换机
    public static final String EXCHANGE_NAME = "seckill.Exchange";
    //路由键
    public static final String ROUTING_KEY_NAME = "seckill.message";
    //    延迟交换机名字
    public static final String DELAYED_EXCHANGE_NAME = "delayed.exchange";
    //    routing key
    public static final String DELAYED_ROUTING_KEY = "delayed.routingkey";
    // 死信队列
    public static final String DELAYED_QUEUE_NAME = "delayed.queue";
}