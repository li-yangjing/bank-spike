package com.example.bankseckill.pojo.vo;

import com.example.bankseckill.pojo.SeckillProduct;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * 公共返回对象枚举
 * 考虑返回常量太多 用枚举类代替常量
 */
@ToString
@AllArgsConstructor
@Getter
public enum RespBeanEnum {

    SUCCESS(200,"SUCCESS"),
    //用户
    UPDATE_AVATER_ERROR(404404,"更新头像失败！"),
    FILE_EMPTY(404404,"图片上传为空"),

// 登录
    ERRPR(500,"服务端异常"),
    PASSWORD_PHONE_INPUT_ERROR(500209,"密码账号输入为空"),
    LOGIN_ERROR(500210,"用户名或密码不正确"),
    LOGIN_MOBILE_ERROR(500211,"手机号码格式错误"),
    BIND_ERROR(500212,"参数校验异常"),
    CAPTCHA_ERROR(500213,"验证码错误!"),
    //注册
    REGISTER_IDENTITY_ERROR(500214,"身份证已被使用"),
    REGISTER_PHONE_ERROR(500214,"手机号已被使用"),
//    jwt
    TOKEN_EXPIRED(500300,"token已经过期"),
    TOKEN_ERROR(500301,"签名错误!"),
    TOKEN_ALGORITH(500302,"加密算法不匹配"),
    TOKEN_EMPTY(500303,"token为空"),
    TOKEN_INVALID(500304,"TOKEN未知错误"),
    TOKEN_LOGIN_EMPTY(500306,"尚未登录,请登录再试!"),
    TOKEN_INVALID_ERROR(500305,"您访问权限不足，请联系管理员"),
    //秒杀产品
    PRODUCT_PARAMETER_ERROR(500404,"参数错误!"),
    KILL_LOGIN_ERROR(400,"您尚未登录!"),
    EMPTY_STOCK(500405,"库存不足!"),
    REPEATE_BUY_ERROR(500406,"您已经抢购了,不能重复购买！"),
    ACCES_LIMIT_ERROR(400,"请求过于频繁，请稍后再试！"),
    Busy_Activities(400,"活动太火爆了,请稍后再试！"),

    //管理员接口
    ADMIN_INVAILD_LOGIN(403403,"权限不足！请联系管理员");
    private final Integer code;
    private final String message;
}
