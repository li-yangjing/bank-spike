package com.example.bankseckill.pojo.vo;


import com.example.bankseckill.Validator.isEmail;
import com.example.bankseckill.Validator.isIdentity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
 * 邮箱验证码实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "邮箱验证码对象",description = "")
public class EmailVo {

    @ApiModelProperty(value = "邮箱地址")
    @NotBlank(message = "邮箱地址不能为空")
    @isEmail
    private String email;

    @ApiModelProperty(value = "身份证号")
    @NotBlank(message = "身份证号不能为空")
    @isIdentity
    private String identitycard;
}
