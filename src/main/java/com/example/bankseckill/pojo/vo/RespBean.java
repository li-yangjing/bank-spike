package com.example.bankseckill.pojo.vo;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 公共对象
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "统一返回对象")
public class RespBean {
    private long code;
    private String message;
    private Object obj;
    //当前时间
    private String time;
    //当前时间戳
    private long timestamp;



    /**
     * 成功返回结果
     * @return
     */
    public static RespBean success(){
        return  new RespBean(RespBeanEnum.SUCCESS.getCode(),RespBeanEnum.SUCCESS.getMessage(), null,new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS").format(new Date()),System.currentTimeMillis());
    }


    /**
     * 成功返回结果 （带对象）
     * @param o
     * @return
     */
    public static RespBean success(Object o){
        return  new RespBean(RespBeanEnum.SUCCESS.getCode(),RespBeanEnum.SUCCESS.getMessage(),o,new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS").format(new Date()),System.currentTimeMillis());
    }

    public static RespBean success(String message,Object o){
        return  new RespBean(RespBeanEnum.SUCCESS.getCode(),message,o,new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS").format(new Date()),System.currentTimeMillis());
    }


    /**
     * 失败返回结果
     * @param respBeanEnum
     * @return
     */
    public static RespBean error(RespBeanEnum respBeanEnum){
        return  new RespBean(respBeanEnum.getCode(),respBeanEnum.getMessage(),null,new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS").format(new Date()),System.currentTimeMillis());
    }

    /**
     * 失败返回结果（带对象）
     * @param respBeanEnum
     * @param o
     * @return
     */
    public static RespBean error(RespBeanEnum respBeanEnum, Object o){
        return  new RespBean(respBeanEnum.getCode(),respBeanEnum.getMessage(),o,new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS").format(new Date()),System.currentTimeMillis());
    }

    /**
     * 自定义回复消息
     * @param message
     * @return
     */
    public static RespBean error(String message){
        return new RespBean(400,message,null,new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS").format(new Date()),System.currentTimeMillis());
    }

}
