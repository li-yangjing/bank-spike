package com.example.bankseckill.pojo.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.example.bankseckill.Validator.isIdentity;
import com.example.bankseckill.Validator.isMobile;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * @Author: liyangjing
 * @Date: 2022/02/15/15:26
 * @Description:
 * 管理员更新用户对象
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "更新用户信息实体类")
public class UpdateUserVo {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id ")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "身份证")
    @NotBlank(message = "身份证不能为空")
    @isIdentity
    private String identitycard;

    @ApiModelProperty(value = "姓名")
    @NotBlank
    private String name;

    @ApiModelProperty(value = "电话")
    @NotBlank(message = "手机号不能为空")
    @isMobile
    private String phone;

    @ApiModelProperty(value = "年龄")
    private Integer age;

    @ApiModelProperty(value = "邮箱")
    @NotBlank(message = "邮箱不能为空")
    @Email
    private String email;

    @ApiModelProperty(value = "用户名")
    @NotBlank(message = "用户名不能为空")
    private String username;

    @ApiModelProperty(value = "风险等级，低0，中1，高2")
    @TableField("risk_level")
    //必须大于2
    @Max(value=2,message = "风险等级 0 1 2")
    private Integer riskLevel;

    @ApiModelProperty(value = "是否工作，1工作，0失业")
    //必须大于1
    @Max(value = 1,message = "只有失业0和工作1中")
    private Integer isinwork;

    @ApiModelProperty(value = "是否失信，1失信，0信誉良好")
    @Max(value = 1,message = "信誉等级 0 1")
    private Integer ishonest;

    @ApiModelProperty(value = "用户状态，1启动，0禁用")
    @Max(value = 1,message = "0 1 启用 禁用")
    private Integer enable;
}
