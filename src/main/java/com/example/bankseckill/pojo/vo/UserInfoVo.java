package com.example.bankseckill.pojo.vo;

import com.example.bankseckill.Validator.isIdentity;
import com.example.bankseckill.Validator.isMobile;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

/**
 * @Author: liyangjing
 * @Date: 2022/02/14/22:13
 * @Description:
 *
 * 用户信息对象（方便修改用户信息）
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "用户信息对象",description = "")
public class UserInfoVo {

    @ApiModelProperty("身份证")
    @NotBlank(message = "身份证不能为空")
    @isIdentity
    private String identitycard;

    @ApiModelProperty("用户名")
    @NotBlank(message = "用户名不能为空")
    private String username;

    @ApiModelProperty("phone")
    @NotBlank(message = "手机号不能为空")
    @isMobile
    private String phone;

    @ApiModelProperty("email")
    @NotBlank(message = "邮箱不能为空")
    @Email
    private String email;


}
