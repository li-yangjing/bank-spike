package com.example.bankseckill.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
 * 支付类型包装类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "支付对象",description = "")
public class AliPay {

    @ApiModelProperty(value = "订单标题")
    @NotBlank(message = "订单标题不能为空")
    private String subject;
    @ApiModelProperty(value = "订单编号")
    @NotBlank(message = "订单编号不能为空")
    private String traceNo;
    @ApiModelProperty(value = "订单金额")
    @NotBlank(message = "订单金额不能为空")
    private String totalAmount;

}
