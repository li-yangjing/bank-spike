package com.example.bankseckill.pojo.vo;


import com.example.bankseckill.Validator.isEmail;
import com.example.bankseckill.Validator.isIdentity;
import com.example.bankseckill.Validator.isMobile;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

/**
 * 注册实体类
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "注册实体类")
public class RegisterVo {

    @ApiModelProperty(value = "身份证",required = true)
    @NotEmpty
    @isIdentity
    private String identitycard;

    @ApiModelProperty(value = "姓名",required = true)
    @NotEmpty
    private String name;

    @ApiModelProperty(value = "电话",required = true)
    @NotEmpty
    @isMobile
    private String phone;

    @ApiModelProperty(value = "邮箱",required = true)
    @NotEmpty
    @isEmail
    private String email;
    /**
     * 大于5为密码
     */
    @ApiModelProperty(value = "密码",required = true)
    @NotEmpty
    private String password;

    @ApiModelProperty(value = "验证码",required = true)
    @NotEmpty
    private String vertifycode;


}
