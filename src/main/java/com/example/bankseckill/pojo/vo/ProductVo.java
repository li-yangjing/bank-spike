package com.example.bankseckill.pojo.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.example.bankseckill.pojo.Product;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: liyangjing
 * @Date: 2022/02/09/22:19
 * @Description:
 * 秒杀商品对象类
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "秒杀商品实体类",description = "")
public class ProductVo extends Product {

    /**
     * 秒杀产品id
     */
    @ApiModelProperty(value = "秒杀商品id")
    private int pid;

    /**
     * 秒杀产品name
     */
    @ApiModelProperty(value = "秒杀商品姓名")
    private String productName;
    /**
     * 秒杀年利率
     */
    @ApiModelProperty(value = "秒杀年利率")
    private Float seckillannualInterestRate;

    /**
     * 库存数量
     */
    @ApiModelProperty(value = "库存数量")
    private Integer stockCount;

    /**
     * 秒杀开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "Asia/Shanghai")
    @ApiModelProperty(value = "秒杀开始时间")
    private Date startDate;

    /**
     * 秒杀结束时间
     */
    @ApiModelProperty(value = "秒杀结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "Asia/Shanghai")
    private Date endDate;

    /**
     * 商品状态 -1已结束 0未开始 1进行中
     */

    @ApiModelProperty(value = "商品状态 -1已结束 0未开始 1进行中")
    @TableField(exist = false)
    private int status;

    /**
     * 活动启动状态 0禁用 1启用
     */
    @ApiModelProperty(value = "商品启用状态  1启用 0禁用")
    @TableField(exist = false)
    private int enable;

    @ApiModelProperty(value ="距离开始时间s" )
    @TableField(exist = false)
    private int remainSeconds;
}
