package com.example.bankseckill.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author Liyangjing
 * @since 2022-03-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_booking_seckill")
public class BookingSeckill implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 预约id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户id
     */
    @TableField("userId")
    private Long userId;

    /**
     * 秒杀产品id
     */
    @TableField("seckillproductId")
    private Long seckillproductId;

    @TableField(exist = false)
    private Product product;

    /**
     * 开始时间
     */
    @TableField("startTime")
    private Date startTime;

    /**
     * 结束时间
     */
    @TableField("endTime")
    private Date endTime;

    /**
     * 预约状态（0 活动即将开始，1活动正在火爆进行中 ，-1活动已结束）
     */
    @TableField("status")
    private Integer status;


}
