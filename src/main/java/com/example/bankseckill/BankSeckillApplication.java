package com.example.bankseckill;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@MapperScan("com.example.bankseckill.mapper")
@EnableScheduling
public class BankSeckillApplication {

    public static void main(String[] args) {
        SpringApplication.run(BankSeckillApplication.class, args);
    }

}
