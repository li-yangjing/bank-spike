package com.example.bankseckill.mapper;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.bankseckill.pojo.SeckillmessageLog;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Liyangjing
 * @since 2022-03-02
 */
@Mapper
public interface SeckillmessageLogMapper extends BaseMapper<SeckillmessageLog> {

}
