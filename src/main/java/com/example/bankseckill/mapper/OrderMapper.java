package com.example.bankseckill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.bankseckill.pojo.Order;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.Date;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liyangjing
 * @since 2022-01-26
 */
@Mapper
public interface OrderMapper extends BaseMapper<Order> {
    /**
     * 用户获取自己所有的订单
     * @param page
     * @return
     */
    IPage<Order> getOrderByPage(Page<Order> page,@Param("id") Long id);

    @Update("update t_order set status = #{status},pay_date = #{payTime} where id = #{tradeNo}")
    void updateState(@Param("tradeNo") int id,@Param("status") int status, @Param("payTime") String paydate);
}
