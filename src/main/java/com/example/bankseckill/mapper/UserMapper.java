package com.example.bankseckill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.bankseckill.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liyangjing
 * @since 2022-01-26
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
    /**
     * 分页得到所有用户信息（管理员）
     * @param page
     * @param username
     * @param phone
     * @param identitycard
     * @return
     */
    IPage<User> getUserInfoByPage(Page<User> page, @Param("username") String username,@Param("phone") String phone,@Param("identitycard") String identitycard);
}
