package com.example.bankseckill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.bankseckill.pojo.Product;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liyangjing
 * @since 2022-01-26
 */
@Mapper
public interface ProductMapper extends BaseMapper<Product> {
    /**
     * 返回所有商品（分页）
     * @param page
     * @param keywords
     * @return
     */
    IPage<Product> getProductByPage(Page<Product> page,@Param("keywords") String keywords);
}
