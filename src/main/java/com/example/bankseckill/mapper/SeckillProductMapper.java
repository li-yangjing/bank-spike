package com.example.bankseckill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.bankseckill.pojo.SeckillProduct;
import com.example.bankseckill.pojo.vo.ProductVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liyangjing
 * @since 2022-01-26
 */
@Mapper
public interface SeckillProductMapper extends BaseMapper<SeckillProduct> {
    /**
     * 返回所有即将开始的秒杀商品(分页)
     * @return
     */
    IPage<SeckillProduct> getSecKillProductByPage(Page<SeckillProduct> page,@Param("keywords") String keywords);
    /**
     * 返回秒杀商品详情
     * @param seckillProductId
     * @return
     */
    ProductVo findProductVoById(Long seckillProductId);
    /**
     * 查询所有的秒杀商品
     * @return
     */
    List<ProductVo> findProductVo();
    /**
     * 返回正在进行的秒杀商品
     * @param page
     * @param keywords
     * @return
     */
    IPage<SeckillProduct> getstartSecKillProductByPage(Page<SeckillProduct> page, String keywords);
    /**
     * 返回所有的秒杀商品
     * @param page
     * @param keywords
     * @return
     */
    IPage<SeckillProduct> getallSecKillProductByPage(Page<SeckillProduct> page, String keywords);
}
