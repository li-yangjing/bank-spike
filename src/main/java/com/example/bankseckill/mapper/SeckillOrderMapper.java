package com.example.bankseckill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.bankseckill.pojo.SeckillOrder;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liyangjing
 * @since 2022-01-26
 */
@Mapper
public interface SeckillOrderMapper extends BaseMapper<SeckillOrder> {
    /**
     * 管理员查看所有秒杀订单(分页)
     * @param page
     * @param keywords
     * @return
     */
    IPage<SeckillOrder> getSecKillOrderByPage(Page<SeckillOrder> page,@Param("keywords") String keywords);
    /**
     * 获取某个秒杀订单信息
     * @param id
     * @return
     */
    SeckillOrder FindSecOrderById(@Param("id") Long id);
}
