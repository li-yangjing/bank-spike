package com.example.bankseckill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.bankseckill.pojo.BookingSeckill;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface BookingSeckillMapper extends BaseMapper<BookingSeckill> {

    /**
     * 返回用户所有秒杀预约记录
     * @return
     */
    List<BookingSeckill> findRecordsByUserId(Long id);
}

