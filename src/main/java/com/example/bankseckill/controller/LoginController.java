package com.example.bankseckill.controller;

import com.example.bankseckill.pojo.User;
import com.example.bankseckill.pojo.vo.LoginUser;
import com.example.bankseckill.pojo.vo.RegisterVo;
import com.example.bankseckill.pojo.vo.RespBean;
import com.example.bankseckill.service.IUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;

/**
 * 登录接口
 * author:lyj
 */
@RestController
@RequestMapping("/bank")
@Api(tags = "登录注册模块")
public class LoginController {

    @Autowired
    private IUserService userService;

    /**
     * 登录
     *
     * @param user
     * @param request
     * @return
     */
    @ApiOperation(value = "登录接口")
    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public RespBean login(@RequestBody @Valid LoginUser user, HttpServletRequest request){
        return  userService.login(user,request);
    }

    /**
     * 注销接口
     * @return
     */
    @ApiOperation(value = "注销接口")
    @RequestMapping(value = "/logout",method = RequestMethod.GET)
    public RespBean logout(User user){
        user.setLastLoginDate(new Date());
        userService.updateById(user);
        return RespBean.success("退出成功!");
    }

    /**
     * 注册接口
     * @return
     */
    @ApiOperation(value = "注册接口")
    @RequestMapping(value = "/register",method = RequestMethod.POST)
    public RespBean register(@RequestBody @Valid RegisterVo registerVo){
        return  userService.register(registerVo);
    }
}
