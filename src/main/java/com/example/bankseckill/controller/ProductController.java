package com.example.bankseckill.controller;


import com.example.bankseckill.pojo.Product;
import com.example.bankseckill.pojo.SeckillProduct;
import com.example.bankseckill.pojo.vo.RespBean;
import com.example.bankseckill.pojo.vo.RespPageBean;
import com.example.bankseckill.service.IProductService;
import com.example.bankseckill.service.ISeckillProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author liyangjing
 * @since 2022-01-26
 */
@Slf4j
@RestController
@RequestMapping("/bank/product")
@Api(tags = "产品模块")
public class ProductController {

    @Autowired
    private IProductService productService;


    /**
     *
     * @param currentPage
     * @param size
     * @param keywords keywords 只针对了 productName
     * @return
     */
    @ApiOperation(value = "返回所有商品（分页）")
    @RequestMapping(value = "/getAllproduct",method = RequestMethod.POST)
    public RespPageBean getAllProduct(@RequestParam(defaultValue = "1")Integer currentPage,
                                      @RequestParam(defaultValue = "10")Integer size,
                                      String keywords){
        return productService.getProductByPage(currentPage,size,keywords);
    }

    /**
     * 返回商品详情
     * @param id
     * @return
     */
    @ApiOperation(value = "返回商品详情")
    @RequestMapping(value = "/product/detail",method = RequestMethod.POST)
    public RespBean getProductdetail(Integer id){
        log.info("传入的参数{}",id);
        Product product = productService.getById(id);
        return RespBean.success(product);
    }

}
