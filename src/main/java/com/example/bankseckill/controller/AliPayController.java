package com.example.bankseckill.controller;


import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.example.bankseckill.config.AccessLimit;
import com.example.bankseckill.mapper.OrderMapper;
import com.example.bankseckill.pojo.Order;
import com.example.bankseckill.pojo.vo.AliPay;
import com.example.bankseckill.service.IOrderService;
import com.example.bankseckill.service.ISeckillOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * 支付宝 回调类
 */
@Slf4j
@Api(tags = "支付模块")
@RestController
@RequestMapping("/bank/alipay")
public class AliPayController {

    @Autowired
    private IOrderService orderService;

    //appid
    private final String APP_ID = "2021000118637694";
    //应用私钥
    private final String APP_PRIVATE_KEY = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDaF+VIZaLFjPZ4i7FbggV8k1UUTaDYWZgTvLGTzvpAjL8q3lF2cKM+Z0h8uZjsVj8BopA1NNZ5mVHsxogeIpudbrqxLyuKWVmZDRqygc9cvKeFRiVDf1/DEYHHsr4h4+F0NPkX2T3Y3e+95yhB9tLY3BlRAfmQRSeE0kathJ9TkrTc8ZTc7DuhqvBXoxonmMbc6YwLv+6RngWGwm9LejYCL7Y/F38BLRjgNwmYd4FPe/pAP/uCE7ZIpKBYtKK7xhsXZhDq5UXB7wi/e2Es1JOOa1DWfmDtFImTJ5Setne/QW1FS3+wR/jEoXz/FnIxMBcQBeac/urnl2gh5nqFKpKpAgMBAAECggEBAJQYu+rAkZqikU/kVEh2b3Fbvt9to3amyF1GujNLzqX8rWfX1eueq0uZ6SBBC/YYzuo2UMTAn4+tLHOjkRUdnRx377CX/nIRIIWM338uJPitZvhaXL2MBiQ5hC5uUyxCSXuW6TtnmtbkRoNnL/AzQpReSZa3x5dPTnmkLLhxoZumduqT9/MG9O1dTZ/ORdv7vDl5HjUXfEQlpvb4RGQatooVa/lwy14hN43ixBkm/XxWNIeYIYPIQmaWpLFfImRWv7BFP9v3fMIjECWtEYbrmw+laP4c0g4W81mAu/6z8oTFO6VpbtB6OCt5LNspA7aobjEi/re8y7N1GhXnsLGAVDECgYEA/0iLERFa3CBW/actjk8W551T6PHTtm2HHqX6R40T8TNa3MEjhCTr1TzwPxWpwonB8tbvfKCI52zhnGEobObguhQ3OlFI+K5yyr1nx1CNMgK6pdsPXQScEV+K5h5cIuVR10qZppc3DMK5kz7kvcC/acdzoyiOXnQZR6p3SUNIsmUCgYEA2rSgTPUW4ZsY8gtdgkK+z+BwADhiMZLmGQG/ZeKQefG4WJNF2XfO/QmpeszUwm3U2m8yA07Z3M2dx0jb8inFrV08yvwSn8xPx7YIuNZaYxvfMJc0XNRcLq7c6QqPlJEWsbU+yuVMxDDiSoQcYZ+da4qjBuIlmQqvSK0JZOVO+PUCgYBgd0w9xd4cwoe80DixxwczvMrhgshq7laZ6UR7Vz9K7QoP01gVblJDwQRUAl6k6aOFnVzgLlWthgJ3MmuxQ0l2Z387JU460skD/bDCq5i/ZjKby/yxPLWxBvaY4K4WbauQfFs1jXy68LZpguGzJqvuT/VNmVy7KooG/86Vu7wz1QKBgCDWXfswEW9uSiCNdEzKmLu9hxOBY7ztwX6ftI0np2mn0XX7JtGwG3QCr1+wwYEv3HWvnGLCxBNTDA2P8vY5L2UISshvcJxtoMzrmAOoq4mV9bzl9wa5G94mgyS6YTR9VFr6S6NLKmuKq29kxp4hyREMj83Ixrxjzk2rLW3owCLBAoGBAN1MHS6CPRWnsNgOvq4CV1IxbcLXYuh4OkFjLT6q2SkRgakSoDS39G+EXGq/8LP3qvE9uZFXBovaRvbAuH8eybH8rQ0lMlk+2vDLuv3238WQ7SBfLbKfKnXk7sQ9POS9p0x7qpqPZDPv1Mfp0lOeUXVZGm2QP7ektFWMUikgDq2Y";
    private final String CHARSET = "UTF-8";
    // 支付宝公钥
    private final String ALIPAY_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhBUe1DRcwWnP+CrS+3rTAxmpBFv6FGyvjWX+av5v+R3k1Qgo9PoaWyGhHAh0tW7T7iqmynF84JXNKeK5VM8t8HUfY0N3GIzJePuhEk0cJnRoVjnDYpl8Xa3wFfEw6Cn11iDSaJgMWNMBPhckA2GjZSnF4qjYOAiGn7oTjVNfvEaibzUaazIKNDxEH40xHLK6ozfJXY1ogKtcmf4jgfxfL7900ql36AbDkWS7XTeKiDPECEI6CZUoaUQ1m8i/QX7+U1ZJmNmhUCV3BhcI1BUQKMjFoYjHt1XLtjqLjkzxvoqLSF5m+EZRAdVt8qXdDFOMRirXGgy8GGpzL0p0K9WmtwIDAQAB";
    //这是沙箱接口路径,正式路径为https://openapi.alipay.com/gateway.do
    private final String GATEWAY_URL ="https://openapi.alipaydev.com/gateway.do";
    private final String FORMAT = "JSON";
    //签名方式
    private final String SIGN_TYPE = "RSA2";
    //支付宝异步通知路径,付款完毕后会异步调用本项目的方法,必须为公网地址
    private final String NOTIFY_URL = "http://81.70.146.198:8081/bank/alipay/notify";
    //支付宝同步通知路径,也就是当付款完毕后跳转本项目的页面,可以不是公网地址
    private final String RETURN_URL = "http://81.70.146.198:8081/bank/alipay/ReturnNotify";

    @GetMapping("/pay")
    @ApiOperation("支付接口,在接口文档打不开需拼接在游览器打开，测试账号在md文件")
    private String sendRequestToAlipay(@Valid AliPay aliPay) throws AlipayApiException {

        Order order = orderService.getById(aliPay.getTraceNo());
        //若支付状态已经超时，不能支付
        if(order.getStatus()!=0){
            return "该订单已经超时，或者取消";
        }
        //获得初始化的AlipayClient
        AlipayClient alipayClient = new DefaultAlipayClient(GATEWAY_URL,APP_ID,APP_PRIVATE_KEY,FORMAT,CHARSET,ALIPAY_PUBLIC_KEY,SIGN_TYPE);

        //设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(RETURN_URL);
        alipayRequest.setNotifyUrl(NOTIFY_URL);

        //商品描述（可空）
        String body="";
        alipayRequest.setBizContent("{\"out_trade_no\":\"" + aliPay.getTraceNo() + "\","
                + "\"total_amount\":\"" + aliPay.getTotalAmount() + "\","
                + "\"subject\":\"" + aliPay.getSubject() + "\","
                + "\"body\":\"" + body + "\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

        //请求
        String result = alipayClient.pageExecute(alipayRequest).getBody();
        return result;
    }


}
