package com.example.bankseckill.controller;


import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.example.bankseckill.Validator.Role;
import com.example.bankseckill.mapper.UserMapper;
import com.example.bankseckill.pojo.BookingSeckill;
import com.example.bankseckill.pojo.Product;
import com.example.bankseckill.pojo.SeckillProduct;
import com.example.bankseckill.pojo.User;
import com.example.bankseckill.pojo.vo.ProductVo;
import com.example.bankseckill.pojo.vo.RespBean;
import com.example.bankseckill.pojo.vo.RespBeanEnum;
import com.example.bankseckill.pojo.vo.RespPageBean;
import com.example.bankseckill.rabbitmq.MQReceiver;
import com.example.bankseckill.service.IBookingSeckillService;
import com.example.bankseckill.service.IProductService;
import com.example.bankseckill.service.ISeckillProductService;
import com.example.bankseckill.utils.JWTUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author liyangjing
 * @since 2022-01-26
 */
@RestController
@RequestMapping("/bank/seckill-product")
@Slf4j
@Api(tags = "秒杀配置类模块")
public class SeckillProductController {

    @Autowired
    private ISeckillProductService seckillProductService;

    @Autowired
    private IProductService productService;

    @Autowired
    private RedisTemplate redisTemplate;


    /**
     * 返回所有即将开始的秒杀商品 (分页)
     * @param currentPage
     * @param size
     * @param keywords 商品名字
     * @return
     */
    @ApiOperation(value = "返回即将开始的秒杀商品")
    @RequestMapping(value = "/",method = RequestMethod.POST)
    public RespPageBean getsecKillProduct(@RequestParam(defaultValue = "1")Integer currentPage,
                                          @RequestParam(defaultValue = "10")Integer size,
                                          String keywords){
        return seckillProductService.getsecKillProductByPage(currentPage,size,keywords);
    }

    /**
     * 返回正在进行的秒杀商品
     * @param currentPage
     * @param size
     * @param keywords
     * @return
     */
    @ApiOperation(value = "返回正在进行的秒杀商品")
    @RequestMapping(value = "/starting",method = RequestMethod.POST)
    public RespPageBean getStartsecKillProduct(@RequestParam(defaultValue = "1")Integer currentPage,
                                          @RequestParam(defaultValue = "10")Integer size,
                                          String keywords){
        return seckillProductService.getstartsecKillProductByPage(currentPage,size,keywords);
    }

    /**
     * 返回所有秒杀商品
     * @param currentPage
     * @param size
     * @param keywords
     * @return
     */
    @Role(value = 1)
    @ApiOperation(value = "返回所有的秒杀商品")
    @RequestMapping(value = "/allproducts",method = RequestMethod.POST)
    public RespPageBean getallsecKillProduct(@RequestParam(defaultValue = "1")Integer currentPage,
                                               @RequestParam(defaultValue = "10")Integer size,
                                               String keywords){
        return seckillProductService.getallsecKillProductByPage(currentPage,size,keywords);
    }

    /**
     * 返回秒杀商品详情
     * @param SeckillProductId
     * @return
     */
    @ApiOperation(value = "返回秒杀产品详情")
    @RequestMapping(value = "/detail",method = RequestMethod.POST)
    public RespBean getSecProductdetail(String SeckillProductId){
        long id = Long.parseLong(SeckillProductId);
        log.info("返回秒杀产品详情 id==>{}",SeckillProductId);
        if(SeckillProductId==null){
            return RespBean.error(RespBeanEnum.ERRPR);
        }
        ProductVo productVo =seckillProductService.findProductVoById(id);
        return RespBean.success(productVo);
    }

    /**
     * 返回秒杀商品的预约人数
     */
    @ApiOperation(value = "返回该产品的预约人数")
    @RequestMapping(value = "/booking/numbers",method = RequestMethod.GET)
    public RespBean getBookingNumbers(String SeckillProductId){
        long id = Long.parseLong(SeckillProductId);
        log.info("返回该产品的预约人数==>{}",SeckillProductId);
        //校验商品是否存在
        ProductVo productVo = seckillProductService.findProductVoById(id);
        if(productVo==null){
            return RespBean.error("该商品不存在");
        }
        Long BooingNumbers = redisTemplate.opsForSet().size("bookingSecproduct:" + SeckillProductId);
        return RespBean.success(BooingNumbers);
    }

    /**
     * 返回该用户是否预约该商品的boolean值
     */
    @ApiOperation(value = "查询用户是否预约该商品")
    @RequestMapping(value = "/booking/user",method = RequestMethod.GET)
    public RespBean getUserBooking(User user,String SeckillProductId){
        long id = Long.parseLong(SeckillProductId);
        log.info("查询用户是否预约该商品==>{}",SeckillProductId);
        //校验商品是否存在
        ProductVo productVo = seckillProductService.findProductVoById(id);
        if(productVo==null){
            return RespBean.error("该商品不存在");
        }
        Boolean Bookingresult = redisTemplate.opsForSet().isMember("bookingSecproduct:" + SeckillProductId, user.getId());
        return RespBean.success(Bookingresult);
    }
}
