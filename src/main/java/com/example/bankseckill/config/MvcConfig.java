package com.example.bankseckill.config;



import com.example.bankseckill.interceptor.JWTInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;


/**
 * webmvc过滤器
 * author：lyj
 */
@Configuration
public class MvcConfig implements WebMvcConfigurer {

    //jwt拦截器
    @Autowired
    private JWTInterceptor jwtInterceptor;

    //参数校验器
    @Autowired
    private UserArgumentResolver resolver;

    //限流拦截器
    @Autowired
    private AccessLimitInterceptor accessLimitInterceptor;
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 自定义拦截器，添加拦截路径和排除拦截路径
        registry.addInterceptor(jwtInterceptor)
                /**
                 * 登录接口不拦截 其余拦截
                 */
                .addPathPatterns("/bank/**")
                .excludePathPatterns("/bank/login")
                .excludePathPatterns("/bank/capthca")
                .excludePathPatterns("/bank/register")
                .excludePathPatterns("/bank/mail")
                .excludePathPatterns("/bank/admin/login")
                .excludePathPatterns("/bank/alipay/**");
                //不拦截产品下的接口
//                .excludePathPatterns("/bank/product/**")
//                .excludePathPatterns("/bank/seckill-product/**");
        registry.addInterceptor(accessLimitInterceptor);
    }


    /**
     * 请求方法添加参数
     * @param resolvers
     */
    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        resolvers.add(resolver);
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
        registry.addResourceHandler("/templates/**").addResourceLocations("classpath:/templates/");
        // 放行swagger
        registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
}
