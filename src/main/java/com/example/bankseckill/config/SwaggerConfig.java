package com.example.bankseckill.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

/**
 * swagger2配置类
 *  author：lyj
 *  访问地址 http://localhost:8081/doc.html
 *  @return
 */

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean//配置docket以配置Swagger具体参数
    public Docket docket() {
        return new Docket(DocumentationType.SWAGGER_2).
                apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.example.bankseckill.controller"))
                .build()
                ;
    }
    private ApiInfo apiInfo() {
        Contact contact = new Contact("秒杀大队", "http://fwwb.org.cn/topic/show/1aa5ff30-41a0-4d05-85f9-19eac7ff8c52", "958390434@qq.com");
        return new ApiInfo(
                "银行秒杀系统", // 标题
                "三湘银行秒杀系统", // 描述
                "v1.0", // 版本
                "http://terms.service.url/组织链接", // 组织链接
                contact, // 联系人信息
                "Apach 2.0 许可", // 许可
                "许可链接", // 许可连接
                new ArrayList<>()// 扩展
        );
    }
}
