package com.example.bankseckill.config;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Author: liyangjing
 * @Date: 2022/02/27/21:04
 * @Description:
 *
 *  限流接口
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface AccessLimit{

    int second();

    int maxCount();

    boolean needLogin() default true;
}
