package com.example.bankseckill.Validator;

import com.auth0.jwt.interfaces.Payload;

import javax.validation.Constraint;
import java.lang.annotation.*;


/**
 * 校验身份证注解
 * author：lyj
 */

@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(
        validatedBy = {IsIdentityValidator.class}
)
public @interface isIdentity {
    boolean required() default true;

    String message() default "身份证输入错误";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
