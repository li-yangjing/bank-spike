package com.example.bankseckill.Validator;

import com.example.bankseckill.utils.ValidatorUtil;
import org.springframework.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class IsIdentityValidator implements ConstraintValidator<isIdentity,String> {

    private boolean required = false;

    @Override
    public void initialize(isIdentity constraintAnnotation) {
        required = constraintAnnotation.required();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        if(required){
            return ValidatorUtil.isIdentityNumber(value);
        }else{
            if(StringUtils.isEmpty(value)){
                return true;
            }else{
                return ValidatorUtil.isIdentityNumber(value);
            }
        }
    }

}
