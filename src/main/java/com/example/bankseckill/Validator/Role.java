package com.example.bankseckill.Validator;

/**
 * @Author: liyangjing
 * @Date: 2022/02/11/12:57
 * @Description:
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/***
 * 角色等级
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Role {
    int value();
}

