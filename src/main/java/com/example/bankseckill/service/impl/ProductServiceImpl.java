package com.example.bankseckill.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.example.bankseckill.mapper.ProductMapper;
import com.example.bankseckill.pojo.Product;
import com.example.bankseckill.pojo.vo.RespBean;
import com.example.bankseckill.pojo.vo.RespPageBean;
import com.example.bankseckill.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liyangjing
 * @since 2022-01-26
 */
@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements IProductService {

    @Autowired
    private ProductMapper productMapper;

    /**
     * 返回所有商品（分页）
     * @param currentPage
     * @param size
     * @param keywords
     * @return
     */
    @Override
    public RespPageBean getProductByPage(Integer currentPage, Integer size, String keywords) {
        //开启分页
        Page<Product> page = new Page<>(currentPage, size);
        IPage<Product> productIPage = productMapper.getProductByPage(page,keywords);
        RespPageBean respPageBean = new RespPageBean(200,productIPage.getTotal(),productIPage.getRecords());
        return respPageBean;
    }

    /**
     * 更换产品图片
     * @param product
     * @param url
     * @return
     */
    @Override
    public RespBean updateProductPicture(Product product, String url) {
        product.setPicture(url);
        int result = productMapper.updateById(product);
        if(result==1){
            return RespBean.success("更新成功！");
        }
        return RespBean.error("更新失败！");
    }
}
