package com.example.bankseckill.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.bankseckill.mapper.BookingSeckillMapper;
import com.example.bankseckill.pojo.BookingSeckill;
import com.example.bankseckill.service.IBookingSeckillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Liyangjing
 * @since 2022-03-04
 */
@Service
public class BookingSeckillServiceImpl extends ServiceImpl<BookingSeckillMapper, BookingSeckill> implements IBookingSeckillService {

    @Autowired
    private BookingSeckillMapper bookingSeckillMapper;

    /**
     * 返回用户秒杀预约记录
     * @return
     */
    @Override
    public List<BookingSeckill> findRecordsByUserId(Long id) {
        return bookingSeckillMapper.findRecordsByUserId(id);
    }
}
