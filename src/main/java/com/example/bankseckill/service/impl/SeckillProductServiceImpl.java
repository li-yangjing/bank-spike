package com.example.bankseckill.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.example.bankseckill.mapper.ProductMapper;
import com.example.bankseckill.mapper.SeckillProductMapper;
import com.example.bankseckill.pojo.Product;
import com.example.bankseckill.pojo.SeckillProduct;
import com.example.bankseckill.pojo.vo.ProductVo;
import com.example.bankseckill.pojo.vo.RespBean;
import com.example.bankseckill.pojo.vo.RespPageBean;
import com.example.bankseckill.service.ISeckillProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liyangjing
 * @since 2022-01-26
 */
@Service
public class SeckillProductServiceImpl extends ServiceImpl<SeckillProductMapper, SeckillProduct> implements ISeckillProductService {

    @Autowired
    private SeckillProductMapper seckillProductMapper;

    /**
     * 返回秒杀商品详情
     * @param seckillProductId
     * @return
     */
    @Override
    public ProductVo findProductVoById(Long seckillProductId) {
        ProductVo productVo = seckillProductMapper.findProductVoById(seckillProductId);
        //秒杀产品活动状态 -1已结束 0未开始 1进行中
        int seckillStatus=0;
        Date startDate = productVo.getStartDate();
        Date endDate = productVo.getEndDate();
        Date nowDate = new Date();
        //秒杀状态
        int secKillStatus = 0;
        //秒杀还未开始
        if (nowDate.before(startDate)) {
            // 0秒杀中
            //剩余开始时间
            productVo.setRemainSeconds((int) ((startDate.getTime()-nowDate.getTime())/1000));
        } else if (nowDate.after(endDate)) {
            // -1秒杀已结束
            secKillStatus = -1;
        } else {
            //1进行中
            secKillStatus = 1;
        }
        productVo.setStatus(seckillStatus);
        return productVo;
    }
    /**
     * 返回所有即将开始的秒杀商品(分页)
     * @param currentPage
     * @param size
     * @param keywords
     * @return
     */
    @Override
    public RespPageBean getsecKillProductByPage(Integer currentPage, Integer size, String keywords) {
        //开启分页
        Page<SeckillProduct> page = new Page<>(currentPage,size);
        //将分页装入mapper层
        IPage<SeckillProduct> seckillProductIPage = seckillProductMapper.getSecKillProductByPage(page,keywords);
        RespPageBean respPageBean =new RespPageBean(200,seckillProductIPage.getTotal(),seckillProductIPage.getRecords());
        return respPageBean;
    }

    /**
     * 查询所有的秒杀商品
     * @return
     */
    @Override
    public List<ProductVo> findProductVo() {
        return seckillProductMapper.findProductVo();
    }


    /**
     * 返回正在进行的秒杀商品(分页)
     * @param currentPage
     * @param size
     * @param keywords
     * @return
     */
    @Override
    public RespPageBean getstartsecKillProductByPage(Integer currentPage, Integer size, String keywords) {
        //开启分页
        Page<SeckillProduct> page = new Page<>(currentPage,size);
        //将分页装入mapper层
        IPage<SeckillProduct> seckillProductIPage = seckillProductMapper.getstartSecKillProductByPage(page,keywords);
        RespPageBean respPageBean =new RespPageBean(200,seckillProductIPage.getTotal(),seckillProductIPage.getRecords());
        return respPageBean;
    }

    /**
     * 返回所有秒杀产品
     * @param currentPage
     * @param size
     * @param keywords
     * @return
     */
    @Override
    public RespPageBean getallsecKillProductByPage(Integer currentPage, Integer size, String keywords) {
        //开启分页
        Page<SeckillProduct> page = new Page<>(currentPage,size);
        //将分页装入mapper层
        IPage<SeckillProduct> seckillProductIPage = seckillProductMapper.getallSecKillProductByPage(page,keywords);
        RespPageBean respPageBean =new RespPageBean(200,seckillProductIPage.getTotal(),seckillProductIPage.getRecords());
        return respPageBean;
    }
}
