package com.example.bankseckill.service.impl;



import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.bankseckill.mapper.SeckillmessageLogMapper;
import com.example.bankseckill.pojo.SeckillmessageLog;
import com.example.bankseckill.service.ISeckillmessageLogService;
import org.springframework.stereotype.Service;


/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Liyangjing
 * @since 2022-03-02
 */
@Service
public class SeckillmessageLogServiceImpl extends ServiceImpl<SeckillmessageLogMapper,SeckillmessageLog> implements ISeckillmessageLogService {

}
