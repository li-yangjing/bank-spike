package com.example.bankseckill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.bankseckill.pojo.BookingSeckill;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Liyangjing
 * @since 2022-03-04
 */
public interface IBookingSeckillService extends IService<BookingSeckill> {

    /**
     * 返回用户秒杀预约记录
     * @return
     */
    List<BookingSeckill> findRecordsByUserId(Long id);
}
