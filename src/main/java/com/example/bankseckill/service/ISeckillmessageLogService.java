package com.example.bankseckill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.bankseckill.pojo.SeckillmessageLog;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Liyangjing
 * @since 2022-03-02
 */
public interface ISeckillmessageLogService extends IService<SeckillmessageLog> {

}
