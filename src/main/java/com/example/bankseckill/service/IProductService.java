package com.example.bankseckill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.bankseckill.pojo.Product;
import com.example.bankseckill.pojo.vo.RespBean;
import com.example.bankseckill.pojo.vo.RespPageBean;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liyangjing
 * @since 2022-01-26
 */
public interface IProductService extends IService<Product> {
    /**
     * 返回所有商品（分页）
     * @param currentPage
     * @param size
     * @param keywords
     * @return
     */
    RespPageBean getProductByPage(Integer currentPage, Integer size, String keywords);

    /**
     * 更换产品图片
     * @param product
     * @param url
     * @return
     */
    RespBean updateProductPicture(Product product, String url);
}
