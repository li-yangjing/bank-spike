package com.example.bankseckill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.bankseckill.pojo.User;
import com.example.bankseckill.pojo.vo.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liyangjing
 * @since 2022-01-26
 */
public interface IUserService extends IService<User> {
    /**
     *  登录接口
     * @param user
     * @return
     */
    RespBean login(LoginUser user, HttpServletRequest request);

    /**
     * 注册接口
     * @param registerVo
     * @return
     */
    RespBean register(RegisterVo registerVo);

    /**
     * 根据token获得用户信息
     * @param request
     * @param response
     * @param token
     * @return
     */
    User getUserByToken(HttpServletRequest request, HttpServletResponse response, String token);
    /**
     *  管理员登录接口
     * @param user
     * @return
     */
    RespBean adminlogin(LoginUser user, HttpServletRequest request);

    /**
     * 修改用户的头像
     * @param user
     * @param url
     * @return
     */
    RespBean updateUserAvatar(User user, String url);

    /**
     * 管理员查看所有用户（分页）
     * @param currentPage
     * @param size
     * @param username
     * @param phone
     * @param identitycard
     * @return
     */
    RespPageBean getAllUserInfo(Integer currentPage, Integer size, String username, String phone, String identitycard);
    /**
     * 管理员更改用户信息
     * @param user
     * @return
     */
}

