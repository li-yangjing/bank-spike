package com.example.bankseckill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.bankseckill.pojo.SeckillOrder;
import com.example.bankseckill.pojo.User;
import com.example.bankseckill.pojo.vo.RespBean;
import com.example.bankseckill.pojo.vo.RespPageBean;

import java.util.List;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liyangjing
 * @since 2022-01-26
 */
public interface ISeckillOrderService extends IService<SeckillOrder> {
    /**
     * 管理员查看所有秒杀订单(分页)
     * @param currentPage
     * @param size
     * @param keywords
     * @return
     */
    RespPageBean getSecKillOrderByPage(Integer currentPage, Integer size, String keywords);

    /**
     *  获取秒杀结果
     * @param user
     * @param seckillProductId
     * @return
     */
    Long getResult(User user, Long seckillProductId);

    /**
     * 获取某个秒杀订单信息
     * @param id
     * @return
     */
    SeckillOrder findSecOrderById(Long id);
}
