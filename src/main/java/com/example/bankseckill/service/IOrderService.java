package com.example.bankseckill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.bankseckill.pojo.Order;
import com.example.bankseckill.pojo.User;
import com.example.bankseckill.pojo.vo.ProductVo;
import com.example.bankseckill.pojo.vo.RespPageBean;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liyangjing
 * @since 2022-01-26
 */
public interface IOrderService extends IService<Order> {
    /**
     * 生成秒杀订单
     * @param user
     * @param productVo
     * @return
     */
    Boolean seckill(User user, ProductVo productVo);

    /**
     * 用户获取自己所有的订单
     * @param currentPage
     * @param size
     * @param user
     * @return
     */
    RespPageBean getOrdersByUserId(Integer currentPage, Integer size, User user);

    /**
     * 创建秒杀地址
     * @param user
     * @param seckillProductId
     * @return
     */
    String createSecKillPath(User user, Long seckillProductId);

    /**
     * 校验秒杀地址
     * @param user
     * @param secproductId
     * @return
     */
    boolean checkSecPath(User user, Long secproductId,String path);
}
