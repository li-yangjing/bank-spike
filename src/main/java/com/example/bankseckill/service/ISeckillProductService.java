package com.example.bankseckill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.bankseckill.pojo.SeckillProduct;
import com.example.bankseckill.pojo.vo.ProductVo;
import com.example.bankseckill.pojo.vo.RespPageBean;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liyangjing
 * @since 2022-01-26
 */
public interface ISeckillProductService extends IService<SeckillProduct> {

    /**
     * 返回秒杀商品详情
     * @param seckillProductId
     * @return
     */
    ProductVo findProductVoById(Long seckillProductId);
    /**
     * 返回所有即将开始的秒杀商品(分页)
     * @param currentPage
     * @param size
     * @param keywords
     * @return
     */
    RespPageBean getsecKillProductByPage(Integer currentPage, Integer size, String keywords);

    /**
     * 查询所有的商品
     * @return
     */
    List<ProductVo> findProductVo();

    /**
     * 返回正在进行的秒杀商品(分页)
     * @param currentPage
     * @param size
     * @param keywords
     * @return
     */
    RespPageBean getstartsecKillProductByPage(Integer currentPage, Integer size, String keywords);

    /**
     * 返回所有的秒杀商品(分页)
     * @param currentPage
     * @param size
     * @param keywords
     * @return
     */
    RespPageBean getallsecKillProductByPage(Integer currentPage, Integer size, String keywords);
}
