package com.example.bankseckill.exception;

import com.example.bankseckill.pojo.vo.RespBeanEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 全局异常处理
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GlobalException extends  RuntimeException {
    private RespBeanEnum respBeanEnum;
}
