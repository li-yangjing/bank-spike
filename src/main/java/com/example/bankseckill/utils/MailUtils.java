package com.example.bankseckill.utils;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 邮箱邮件发送工具类
 * author:lyj
 */
@Component
public class MailUtils {

    @Resource
    JavaMailSenderImpl  mailSender;

    public  void MailMessage(String code,String getter) {
        //邮件设置1：一个简单的邮件
        SimpleMailMessage message = new SimpleMailMessage();
        message.setSubject("[三湘银行]");
        message.setText("[三湘银行]注册验证码"+code+",您正在注册账号，五分钟内有效，请妥善保管账户信息");

        message.setTo(getter);
        message.setFrom("958390434@qq.com");
        mailSender.send(message);
    }
}
