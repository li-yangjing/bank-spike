package com.example.bankseckill.utils;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * MD5工具类
 * author：lyj
 */
public class MD5Util {

    public  static  String md5(String src){
        return DigestUtils.md5Hex(src);
    }
//    盐
    private static final String SALT="1a2b3c4d";
    //从前端到后端
    public static String inputPassToFromPass(String inputPass){
        String str = ""+SALT.charAt(0)+SALT.charAt(2)+inputPass+SALT.charAt(5)+SALT.charAt(4);
        return md5(str);
    }
    //后端到数据库
    public  static  String formPassToDBpass(String formPass,String salt){
        String str =""+salt.charAt(0)+salt.charAt(2)+formPass+salt.charAt(5)+salt.charAt(4);
        return md5(str);
    }

    public static  String inputPassToDBPass(String inputPass,String salt){
        String formPass = inputPassToFromPass(inputPass);
        String dBpass = formPassToDBpass(formPass, salt);
        return dBpass;
    }

    public static void main(String[] args) {
//        d3b1294a61a07da9b49b6e22b2cbd7f9 前端转给后端 盐1a2b3c4d
        System.out.println(inputPassToFromPass("123456"));
//        b7797cce01b4b131b433b6acf4add449  后端给数据库
        System.out.println(formPassToDBpass("2a19bfbf9f08385246a16079bdbe167b","1a2b3c4d"));
//         b7797cce01b4b131b433b6acf4add449
        System.out.println(inputPassToDBPass("123456","1a2b3c4d"));
    }

}
