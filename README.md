## 项目进度书

## 一、开发计划书



*<font color='red'>每个人写的代码必须标明注释解释</font>*

技术选型（待定） ： mybaitis-plus  springboot jwt swagger2 redis rabbitmq fastdfs

**考虑到没有过多的角色划分：我们就只使用jwt来做安全校验。**


必须用token令牌来访问接口才能通过

### 1.前端到后端密码加密 后端到数据库密码加密
  考虑到密码安全问题 
  前端在输入密码转给  会进行一次md5加密 ，
  后端到数据库会进行一次加密。


### 2.用户页面

  - 登录（<font color='green'>完成</font>）
      - 手机号+密码+验证码 登录
      - 验证码
      - 加密密码（不能用明文传输）
  - 注册（<font color='green'>完成</font>）
    - 邮箱或者手机号发送验证码注册
    - 身份证、手机号校验
  - 注销（<font color='green'>完成</font>）
  - 更新用户信息（头像）（<font color='green'>完成</font>）
  - 更改用户密码（<font color='green'>完成</font>）
  - 秒杀日志(<font color='red'>未完成</font>)
      - 产品详情。
      - 产品开始时间。




### 3.商品页面

- 查看秒杀产品（进行中和未开始）（<font color='green'>完成</font>）
- 查看所有普通产品（<font color='green'>完成</font>）
- 查看产品详情（<font color='green'>完成</font>）
- 用户预约秒杀商品（<font color='red'>未完成</font>）

  - 产品页面可以查看==预约人数==
  - 用户只能点击一次预约之后不能点击
  - 用户可以查看自己的==预约日程==
  - ==预约时间==开始了可以给用户发送信息（难点）
    - 选用redis的数据类型（==set==？List？发布订阅？）
    - 新建一张用户预约表
    - set 集合返回人数


### 4.订单页面

- 生成秒杀订单（<font color='red'>**秒杀接口**</font>）（<font color='green'>完成部分</font>）

目前做了简单的支付接口，但未处理并发问题（超卖（<font color='green'>完成</font>），重复购买（<font color='green'>完成</font>），未使用redis（<font color='green'>完成</font>），未加入rabbitmq处理支付接口）


- 查看用户订单（<font color='red'>已做</font>）



### 5.管理页面

- 管理员登录接口（<font color='green'>完成</font>）

​	与Use登录接口一样，但在登录时判断admin是否为1，前端可以重新设置一个登录页面为管理员专用

- 商品

  - 删除产品（<font color='green'>完成</font>）
  - 批量删除产品（<font color='green'>完成</font>）
  - 新增产品（更换产品图片）（<font color='green'>完成</font>）
  - 更新产品信息（<font color='green'>完成</font>）

- 用户

  - 查看所有用户信息，除去自身管理员（<font color='green'>完成</font>）
  - 删除和批量用户（<font color='green'>完成</font>）
  - 编辑用户信息（<font color='green'>完成</font>）

- 秒杀商品

  - 可以设置活动的 **开始 数量 价格**（<font color='green'>完成</font>）

  - 可以查看秒杀活动（包括**未开始**,**进行中**,**已结束**）（<font color='green'>完成</font>）

  - 可以查看秒杀产品的订单（<font color='green'>完成</font>）

  - 可以删除秒杀产品（<font color='green'>完成</font>）

  - 可以更新秒杀产品信息（<font color='green'>完成</font>）

    






## 二、测试计划书

> 测试并发量只针对了 秒杀产品接口

测试文件为 秒杀接口.jmx

### 1.测试一人秒杀一件商品（限购）

![image-20220212163938615](E:\2022waiibao\gitee\bankproject\README.assets\image-20220212163938615.png)

一百个线程 测试

查看数据库订单，发现十个库存全部卖完了

![image-20220212164022215](E:\2022waiibao\gitee\bankproject\README.assets\image-20220212164022215.png)

<font color='red'> 限购失败</font> ，需要重新设计

**解决方法**： 加 唯一索引 在秒杀订单表 根据 <font color='red'>userid 和 product id</font> 加唯一索引

<h1>添加唯一索引后 </h1> 
<font color='red'>限购成功</font> 

![image-20220212201731770](E:\2022waiibao\gitee\bankproject\README.assets\image-20220212201731770.png)

![image-20220212201752705](E:\2022waiibao\gitee\bankproject\README.assets\image-20220212201752705.png)

![image-20220212201801633](E:\2022waiibao\gitee\bankproject\README.assets\image-20220212201801633.png)



### 2.多用户测试超卖情况

准备一个工具类 生成一千个用户并得到token（**使用工具类时，需要去掉验证码和校验等功能**）

![image-20220214142634408](E:\2022waiibao\gitee\bankproject\README.assets\image-20220214142634408.png)

生成的一千个token

![image-20220214142856207](E:\2022waiibao\gitee\bankproject\README.assets\image-20220214142856207.png)

**开始测试**

以一号产品为例 有十个库存 我们先用1000个用户同时去抢着十个产品 看能否成功！

![image-20220214143234365](E:\2022waiibao\gitee\bankproject\README.assets\image-20220214143234365.png)

<font color='red'>查看结果,失败!，数据库生成了两百多条数据，这是严重不行的！超卖了190个产品</font>

![image-20220214143439722](E:\2022waiibao\gitee\bankproject\README.assets\image-20220214143439722.png)

**思考：为什么会超卖呢?**

<font color='red'>发现在不断product为0时，没有对生成订单做判断，如果为0，也会生成订单,随后对sql语句做了调整</font>

再次用1000个用户测试 ，发现没有超卖

![image-20220214150931978](E:\2022waiibao\gitee\bankproject\README.assets\image-20220214150931978.png)

<font color='red'>**但出现了新的问题,吞吐量不达标（至少200）**</font>

![image-20220214151111934](E:\2022waiibao\gitee\bankproject\README.assets\image-20220214151111934.png)

### 3.秒杀优化

优化方向：

- 通过redis预减库存，减少数据库的访问(**完成**)
  - ==在初始化时将项目内的库存加入到redis里面，为了防止多次访问数据库==
  - 在用户购买秒杀商品时，先会访问redis里面的库存，如果库存为零，则会终止购买

- 内存标记，减少redis访问（**完成**）
  - ==多次访问内存，也是不合理的==
  - 可以通过内存标记的方式来控制内存的访问，通过map储存商品id，若库存为零，修改map的value值。

- 请求进入异步缓存，队列下单（RabbitMQ）
  - 通过消息发送给消费者生成订单
  - ==因为消息发送需要时间，所以写了访问接口查询，可以轮询查询秒杀成功==


**优化之后的测试**

<font color='red'>无超卖，单个用户也无法购买多次商品</font>

![image-20220225213648967](E:\2022waiibao\gitee\bankproject\README.assets\image-20220225213648967.png)

<font color='red'>QPS：947！！！！</font> 达到并发200以上。

![image-20220225213707413](E:\2022waiibao\gitee\bankproject\README.assets\image-20220225213707413.png)

==优化成功！==

上面是一万并发量的时候

==超过十万并发 电脑直接oom了无法测试==

> 随后我用一万并发 测试了十次

![image-20220225214937153](E:\2022waiibao\gitee\bankproject\README.assets\image-20220225214937153.png)

**吞吐量达到了1000**

**项目也没有出现超卖等情况**



### 4.优化redis操作库存（未做）

上面的代码实际演示会发现Redis的库存有问题，原因==在于Redis没有做原子性，我们采用锁去解决==。



![](E:\2022waiibao\gitee\bankproject\README.assets\image-20220225200806124.png)

分布式锁的问题：

1. 锁过程出现异常怎么办（<font color='red'>三种解决方法</font>）
   1. ==设置过期时间==（也会出现问题，线程乱删除锁）
   2. 方法： 用随机值设置value
      1. 先加锁（设置随机值）
      2. 比较锁的值（==若相同则就是自己的锁，不同就是别人的锁==）

​		 3.Lua脚本设置分布式锁：

- 在服务器端
  - 优点：在后期使用直接调用
  - 缺点：修改不方便，占用资源
- 在java端：
  - 优点：灵活
  - 缺点：每次都要发送lua脚本去服务器端，可能会造成网络通信延长

<font color='red'>使用了redis锁 可以保持可靠性,但会降低并发</font>



详情：[(142条消息) 如何用Redis实现分布式锁_Hopefully Sky的博客-CSDN博客_redis分布式锁](https://blog.csdn.net/fuzhongmin05/article/details/119251590)



### 5.限制恶意请求

- 利用注解 计数器限流（<font color='red'>已做</font>）==缺点：如果用户只在前期抢购，后期不抢购，就会造成服务器后期资源的浪费==

  

漏桶算法（水太大，资源浪费）

计数器算法（消耗资源）

令牌桶算法（每次从桶里面拿token）

### 6.隐藏秒杀接口

秒杀前将会请求两个接口

- 第一个接口返回==秒杀路径Path==。
- 必须将==秒杀路径==拼接在==秒杀接口==的==url的正确位置==，并且校验成功才能秒杀。



## 三、数据库修改

- 产品 没有加图片字段（**完成**）
- 加一张贷款表（**未完成**）
- 用户图片（**完成**）
- 在订单表里加入 订单编号id（未完成）
  - 

​	

## 四、支付宝沙箱测试账号

| 买家账号 | lwkdhw5572@sandbox.com |
| -------- | ---------------------- |
| 登录密码 | 111111                 |
| 支付密码 | 111111                 |





## 五、如何保证消息的幂等性

文章：[(149条消息) 幂等性原理_cristianoxm的博客-CSDN博客_幂等性原理](https://blog.csdn.net/cristianoxm/article/details/113794171)

>  场景问题： 1.若库存已经扣减，在消息发送过程中，网络出现中短延迟，导致消息没有被消费
>
> ​				2.若用户成功发送两次消息，如何做到幂等性

![image-20220303094302306](E:\2022waiibao\gitee\bankproject\README.assets\image-20220303094302306.png)

​	解决方案：方案一（<font color='red'>设置唯一索引 解决消息幂等性</font>）

​	方案二：

- 新增消息日志表
- 通过==唯一索引==设置==消息的幂等性==
- 通过==消息日志表==设置==消息发送失败后的重传==

![image-20220303094839909](E:\2022waiibao\gitee\bankproject\README.assets\image-20220303094839909.png)

> 思路 ：1.将消息设置为持久化，
>
> 2.在每次发送消息前，在数据库里面插入一条消息日志，status为0投递中，
>
> 3.ack回调确认消息是否发送成功，若成功更新消息日志status为1，
>
> 4.若失败，则不更新status
>
> 5.在定时任务Task里面定时检查status为0的消息日志，开启重发，若次数超过3则不发了。

​	**新增Task类**





## 六、解决的BUG

- 更新用户信息bug（缓存问题）<font color='red'> 成功</font>
- 图片上传bug（<font color='red'> 成功</font>）client.conf 文件没有上传

![image-20220304091808647](E:\2022waiibao\gitee\bankproject\README.assets\image-20220304091808647.png)

- （2022.3.4）修改获取秒杀地址的BUG（产品id没有的情况）
- (2022.3.5)消费消息失败！ （id product_id）

![image-20220305213043629](E:\2022waiibao\gitee\bankproject\README.assets\image-20220305213043629.png)

- (2022.3.5)系统初始化时，可以把商品库存数量加载到Redis里面（<font color='red'>加载了产品错误的秒杀id</font>）
- （2022.3.7）如果产品预约时间已经到了，应清空缓存。（==未修改==）
- (2022.3.11) haspMap内存标记 错把 产品id当做==秒杀id==

![image-20220311222613905](E:\2022waiibao\gitee\bankproject\README.assets\image-20220311222613905.png)





## 七、风险决策引擎



用户星级

- 星级根据一下判断

  - 失业 （1颗星）

  - 信誉（2颗星）

  - 预期记录（2颗星）

- ==用户年龄==

产品星级

- 管理员自己设定 星级
- ==产品购买年龄==
- 
缓存产品等级（0-10）
- 用户风险等级<=产品要求等级允许秒杀&&
- 校验接口：获取秒杀地址，秒杀产品  管理员更新产品的同时刷新产品等级缓存

## 八、支付流程

### 1.回调接口 

==为挂入公网==

> 支付成功后**修改订单状态**   可以跳转 页面



### 2.同步接口

==已做==

> 支付成功后可以跳转 页面



### 3.订单生成成功，30分钟未支付取消订单，十分钟内发送邮件提醒

> 技术栈：rabbitmq 的延迟队列死信队列来做

思路：当用户成功下单，存入数据库，并发送消息rabbitmq的队列，三十分钟后，检查数据库的订单状态，若支付则不做任何处理，若未支付则修改状态为订单已取消

==订单状态==

- 0新建未支付（还未支付，在订单生成三十分钟内）
- -1订单已取消（自己取消订单，或者三十分钟未支付）
  - 返回库存
  - 删除订单信息，秒杀订单信息（防止用户未支付后，继续购买）

- 1已支付 （已支付）



- [ ] ==订单生成 ，三十分钟超时未支付，返回库存==
- [ ] 删除秒杀订单,删除Redis的限制
- [ ] 返回库存（Redis，MySQL）







## 九、线上测试秒杀接口

> 高并发下会请求失败！

![image-20220311194344284](E:\2022waiibao\gitee\bankproject\README.assets\image-20220311194344284.png)





> 只有支付成功才扣数据库库存
>
> 秒杀成功就预减库存，



用户下单--》预减库存----》rabbitmq发送----》存入数据库---》





## 2022.3.13

- [ ] 商品已经到结束时间，仍能购买
- [ ] 超时订单消息





